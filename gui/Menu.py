#!/usr/bin/python3

from PyQt4 import QtGui
from . import MenuScene

class Menu(QtGui.QGraphicsView):

    def __init__(self, parent):
        super(Menu, self).__init__()
        self.__parent = parent
        self.__name = "menu"
        self.__sc = MenuScene.MenuScene(self)
        self.setScene(self.__sc)

    def getName(self):
        return self.__name

    def getMenuParent(self):
        return self.__parent

    def getScene(self):
        return self.__sc
