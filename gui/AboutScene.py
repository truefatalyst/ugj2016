#!/usr/bin/python3

from PyQt4 import QtGui, QtCore
from . import Button
from . import Font

class AboutScene(QtGui.QGraphicsScene):

    def __init__(self, parent):
        super(AboutScene, self).__init__(5, 5, 1110, 620)
        self.__parent = parent

        a = Button.Button(
            self.getAboutSceneParent().getAboutParent(),
            "exitm",
            274,
            74,
            self.getAboutSceneParent().getAboutParent().returnToMenu
        )
        a = self.addWidget(a)
        a.setPos(800, 525)

        a = QtGui.QTextEdit()
        a.setFixedHeight(450)
        a.setFixedWidth(1020)
        f = QtGui.QFont("Candara")
        f.setBold(True)
        f.setItalic(True)
        f.setPixelSize(25)
        a.setFont(f)
        with open("readme.txt") as fin:
            a.setText(fin.read())
        a.setReadOnly(True)
        a.setStyleSheet(
            """
            QTextEdit{
                background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #27349e, stop: 1 #55238f);
                color: white;
            }
            """
        )
        a = self.addWidget(a)
        a.setPos(50, 50)

    def getAboutSceneParent(self):
        return self.__parent