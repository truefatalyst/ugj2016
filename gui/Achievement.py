#!/usr/bin/python3

from PyQt4 import QtGui, QtCore
from . import Font

class Achievement(QtGui.QFrame):
    def __init__(self, name, about, pic, block):
        super(Achievement, self).__init__()
        self.__name = name
        self.__about = about
        self.__pic = pic
        self.setFixedWidth(450)
        self.setFixedHeight(250)
        self.__lay = QtGui.QGridLayout()
        self.setStyleSheet(
            """
            QFrame{
                border: 6px solid black;
                background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #1b2370, stop: 1 #3d1868);
            }
            QLabel{
                border: 0px solid black;
            }
            """
        )
        self.setLayout(self.__lay)
        f = Font.Font("Candara")
        f.setBold(True)
        f.setItalic(True)
        #f.find(400, 50, self.__name)
        f.setPointSize(18)
        l = QtGui.QLabel(self.__name)
        l.setFont(f)
        l.setAlignment(QtCore.Qt.AlignCenter)
        l.setStyleSheet("color: white;")
        self.__lay.addWidget(l, 0, 0, 1, 2)

        l = QtGui.QLabel()
        l.setFixedHeight(170)
        l.setFixedWidth(170)
        if not block:
            l.setPixmap(QtGui.QPixmap("resource/achivlock.png"))
        else:
            l.setPixmap(QtGui.QPixmap(pic))
        self.__lay.addWidget(l, 1, 0)

        f.setPointSize(17)
        l = QtGui.QTextEdit(self.__about)
        l.setFont(f)
        l.setAlignment(QtCore.Qt.AlignCenter)
        l.setStyleSheet(
            """
                color: white;
                border-radius: 20px;
                background-color: rgb(0, 0, 0, 90);
            """
        )
        l.setReadOnly(True)
        self.__lay.addWidget(l, 1, 1)
