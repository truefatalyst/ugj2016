#!/usr/bin/python3

from PyQt4 import QtGui
from . import LevelScene

class Level(QtGui.QGraphicsView):
    def __init__(self, parent):
        super(Level, self).__init__()
        self.__name = "level"
        self.__parent = parent
        self.__sc = LevelScene.LevelScene(self)
        self.setScene(self.__sc)

    def getName(self):
        return self.__name

    def getLevelParent(self):
        return self.__parent

    def getScene(self):
        return self.__sc
