#!/usr/bin/python3

from PyQt4 import QtGui
from . import ExtraScene

class Extra(QtGui.QGraphicsView):

    def __init__(self, parent):
        super(Extra, self).__init__()
        self.__parent = parent
        self.__sc = ExtraScene.ExtraScene(self)
        self.__name = "extra"
        self.setScene(self.__sc)

    def getName(self):
        return self.__name

    def getExtraParent(self):
        return self.__parent

    def getScene(self):
        return self.__sc
