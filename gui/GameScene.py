#!/usr/bin/python3

from PyQt4 import QtGui, QtCore
from . import Button
from . import Font
from engine import ngn
from . import Achievement

class GameScene(QtGui.QGraphicsScene):

    def __init__(self, parent, block, level, lvc):
        super(GameScene, self).__init__(5, 5, 1110, 620)
        self.__parent = parent
        self.__block = block
        self.__level = level
        self.__lvc = lvc
        self.__hist = None
        self.__flanim = False
        self.__animobj = None
        self.__anim = []
        self.__valueblock = []
        self.__constitems = []
        self.__funcblock = []
        self.engine = ngn.NGN()
        self.engine.loadfromfile(block, level)
        self.__hint = None
        self.__hinti = -1
        self.__hintbut = None
        self.init()
        self.__timer = self.startTimer(30)

    def animation(self):
        if self.__animobj[3]:
            if self.__animobj[2] <= 370:
                self.__animobj[3] -= 1
            else:
                self.__animobj[2] -= 5
        else:
            self.__animobj[2] += 5
        self.__animobj[0].setPos(self.__animobj[1], self.__animobj[2])
        if self.__animobj[2] == 630:
            self.removeItem(self.__animobj[0])
            return False
        else:
            return True

    def timerEvent(self, *args, **kwargs):
        if self.__flanim:
            self.__flanim = self.animation()
            return
        if self.__anim:
            self.__animobj = None
            i = self.__anim.pop(0)
            ach = self.__lvc.getAchiv()
            arh = ach.getArchive()
            a = Achievement.Achievement(i, arh[i][0], arh[i][1], True)
            a = self.addWidget(a)
            a.setPos(550, 630)
            self.__animobj = [a, 660, 630, 50]
            self.__flanim = True


    def getGameSceneParent(self):
        return self.__parent

    def init(self):
        a = self.addPixmap(QtGui.QPixmap("resource/block.png"))
        a.setPos(25,10)
        a = self.addPixmap(QtGui.QPixmap("resource/blockhint.png"))
        a.setPos(25, 100)
        a.setZValue(-1)
        a = self.addPixmap(QtGui.QPixmap("resource/cellsblock.png"))
        a.setPos(25, 180)
        a.setZValue(-1)
        a = self.addPixmap(QtGui.QPixmap("resource/constblock.png"))
        a.setPos(775, 180)
        a.setZValue(-1)
        self.renderConst(-1)
        self.renderCells()
        self.renderHint()
        self.renderOther()
        self.renderFunc()

    def renderOther(self):
        a = Button.Button(
            self.getGameSceneParent().getGameParent(),
            "checkf",
            274,
            74,
            self.checkF
        )
        a = self.addWidget(a)
        a.setPos(50, 530)
        self.__chf = a
        a = Button.Button(
            self.getGameSceneParent().getGameParent(),
            "checkn",
            274,
            74,
            self.checkN
        )
        a = self.addWidget(a)
        a.setPos(350, 530)
        self.__chn = a
        a = Button.Button(
            self.getGameSceneParent().getGameParent(),
            "exit",
            274,
            74,
            self.stop
        )
        a = self.addWidget(a)
        a.setPos(790, 530)
        a = QtGui.QComboBox()
        f = Font.Font("Candara")
        f.setBold(True)
        f.setItalic(True)
        f.find(200, 45, "Эксперименты")
        size = QtGui.QFontMetrics(f).size(QtCore.Qt.TextSingleLine, "Эксперименты")
        a.setFixedHeight(size.height())
        a.setFixedWidth(size.width()+25)
        a.setInsertPolicy(0)
        a.setCurrentIndex(0)
        a.addItem("Эксперименты")
        a.setEditable(False)
        a.setMaxVisibleItems(5)
        a.setFont(f)
        self.__hist = a
        self.connect(self.__hist, QtCore.SIGNAL('currentIndexChanged(int)'), self.historyChange)
        a = self.addWidget(a)
        a.setPos(850, 110)

    def stop(self):
        self.killTimer(self.__timer)
        self.getGameSceneParent().getGameParent().switchToLevels()

    def historyChange(self):
        cur = self.__hist.currentIndex()
        if not cur:
            return
        cnt = self.__hist.count()
        cur = cnt - cur
        self.renderConst(cur)
        fn = self.engine.gettry(cur)
        for i in range(15):
            self.__valueblock[i].setText(str(fn[0][i]))
            self.__valueblock[i].setStyleSheet(
                """
                QLineEdit {
                    background-color: white;
                }
                QLineEdit:hover{
                    border: 1px solid black;
                    background-color: white;
                }
                """
            )
        fn = self.engine.getguessed(fn)
        for i in fn:
            self.__valueblock[i].setStyleSheet(
                """
                QLineEdit {
                    background-color: lightgreen;
                }
                QLineEdit:hover{
                    border: 1px solid black;
                    background-color:lightgreen;
                }
                """
            )


    def checkF(self):
        u = []
        for i in self.__funcblock:
            u.append(i.currentText())
        self.__anim.extend(self.__lvc.getAchiv().checkPink("".join(u)))
        if self.engine.check_function(u):
            a = self.addPixmap(QtGui.QPixmap("resource/pass.png"))
            a.setPos(30, 30)
            for i in self.__funcblock:
                i.setStyleSheet("background-color: lightgreen")
            self.__chf.hide()
            self.__chn.hide()
            self.__lvc.openLevel(self.__block, self.__level)
            self.__anim.extend(self.__lvc.getAchiv().checkAfter())
            self.__anim.extend(self.__lvc.getAchiv().checkTesn(self.engine.gethistory()))

    def checkN(self):
        ul = []
        u = ""
        for i in self.__valueblock:
            st = i.text()
            u += st
            try:
                t = int(st)
                ul.append(t)
            except ValueError:
                ul.append(0)
                i.setText("0")
        self.__anim.extend(self.__lvc.getAchiv().checkPink(u))
        self.engine.fillstat(ul)
        fn = self.engine.getguessed(self.engine.gettry(len(self.engine.gethistory())))
        for i in range(15):
            self.__valueblock[i].setStyleSheet(
                """
                    QLineEdit {
                        background-color: white;
                    }
                    QLineEdit:hover{
                        border: 1px solid black;
                        background-color: white;
                    }
                    """
            )
        for i in fn:
            self.__valueblock[i].setStyleSheet(
                """
                QLineEdit {
                    background-color: lightgreen;
                }
                 QLineEdit:hover{
                    border: 1px solid black;
                    background-color:lightgreen;
                }
                """
            )
        self.renderConst(len(self.engine.gethistory()))
        self.__hist.setCurrentIndex(0)
        self.__hist.insertItem(1, "№ {}".format(self.__hist.count()))
        self.__anim.extend(self.__lvc.getAchiv().checkAsp(self.__hist.count()-1))

    def renderConst(self, n=-1):
        for i in self.__constitems:
            self.removeItem(i)
        self.__constitems = []
        if n==-1:
            fn = self.engine.getparam()
        else:
            fn = self.engine.gettry(n)[1]
        f = Font.Font("Candara")
        f.setBold(True)
        f.setItalic(True)
        x = 800
        y = 200
        for i in self.engine.forchoice:
            if i in fn:
                t = "{} = {}".format(i, fn[i])
                f.find(270, 40, t)
                a = QtGui.QGraphicsTextItem(t)
                a.setToolTip(self.engine.choicelist[i])
                a.setFont(f)
                a.setDefaultTextColor(QtGui.QColor("white"))
                a.setPos(x, y)
                a.setZValue(-1)
                self.addItem(a)
                self.__constitems.append(a)
                y += 40


    def renderCells(self):
        fn = self.engine.getfunc()[2]
        xr = x = 65
        yr = y = 195
        f = Font.Font("Candara")
        f.setItalic(True)
        f.setBold(True)
        for i in range(3):
            for j in range(5):
                a = self.addPixmap(QtGui.QPixmap("resource/blockcell.png"))
                a.setPos(x, y)
                t = "x = {}".format(fn[i*5+j])
                a = QtGui.QGraphicsTextItem(t)
                f.find(70, 35, t)
                size = QtGui.QFontMetrics(f).size(QtCore.Qt.TextSingleLine, t)
                a.setDefaultTextColor(QtGui.QColor("white"))
                a.setFont(f)
                a.setPos(x+(70-size.width())//2+20, y )
                self.addItem(a)
                a = QtGui.QLineEdit()
                a.setFixedHeight(45)
                a.setFixedWidth(70)
                f.find(70, 45, "1")
                a.setFont(f)
                self.__valueblock.append(a)
                a = self.addWidget(a)
                a.setPos(x+20,y+40)
                x += 137
            x = xr
            y += 110


    def renderHint(self):
        if self.__hint:
            self.removeItem(self.__hint)
        fh = self.engine.getfunc()[3]
        self.__hinti = (self.__hinti + 1) % len(fh)
        f = Font.Font("Candara")
        f.setBold(True)
        f.setItalic(True)
        f.find(700, 45, fh[self.__hinti])
        a = QtGui.QGraphicsTextItem(fh[self.__hinti])
        a.setFont(f)
        a.setDefaultTextColor(QtGui.QColor("white"))
        y = 97+(60-QtGui.QFontMetrics(f).size(QtCore.Qt.TextSingleLine, fh[self.__hinti]).height())//2
        a.setPos(50, y)
        self.__hint = a
        a.setZValue(-1)
        self.addItem(a)
        if not self.__hintbut:
            a = Button.Button(
                self,
                "rhint",
                63,
                63,
                self.renderHint
            )
            a = self.addWidget(a)
            a.setPos(775, 100)
            self.__hintbut = a

    def renderFunc(self):
        f = Font.Font("Candara")
        f.setItalic(True)
        f.setBold(True)
        lf = self.engine.getfunc()
        st = ""
        for i in range(len(lf[0])):
            if i in lf[1]:
                st += " {} ".format(lf[0][i])
            else:
                st += "mxcntm"
        f.find(850, 60, st)
        x = 200
        y = 10
        for i in range(len(lf[0])):
            if i in lf[1]:
                x, y = self.renderFixed(lf[0][i], f, x, y)
            else:
                x, y = self.renderActive(f, x, y)

    def renderFixed(self, st, f, x, y):
        a = QtGui.QGraphicsTextItem(st)
        size = QtGui.QFontMetrics(f).size(QtCore.Qt.TextSingleLine, " {} ".format(st))
        a.setFont(f)
        a.setDefaultTextColor(QtGui.QColor("white"))
        a.setPos(x, y+(80-size.height())//2-5)
        x += size.width()
        self.addItem(a)
        return x, y

    def renderActive(self, f, x, y):
        a = QtGui.QComboBox()
        size = QtGui.QFontMetrics(f).size(QtCore.Qt.TextSingleLine, "mxcntm")
        a.setFixedWidth(size.width())
        a.setFixedHeight(size.height())
        self.editToBox(a)
        self.__funcblock.append(a)
        a.setFont(f)
        a = self.addWidget(a)
        a.setPos(x, y+(80-size.height())//2)
        x += size.width()
        return x, y

    def editToBox(self, box):
        box.setInsertPolicy(0)
        for i in self.engine.forchoice:
            box.addItem(i)
        box.setCurrentIndex(-1)
        box.setEditable(True)
        box.setMaxVisibleItems(5)

