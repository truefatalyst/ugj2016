#!/usr/bin/python3

from PyQt4 import QtGui
from . import AboutScene

class About(QtGui.QGraphicsView):

    def __init__(self, parent):
        super(About, self).__init__()
        self.__parent = parent
        self.__sc = AboutScene.AboutScene(self)
        self.__name = "about"
        self.setScene(self.__sc)

    def getName(self):
        return self.__name

    def getAboutParent(self):
        return self.__parent

    def getScene(self):
        return self.__sc
