#!/usr/bin/python3

from PyQt4 import QtGui
from . import Button


class MenuScene(QtGui.QGraphicsScene):

    def __init__(self, parent):
        super(MenuScene, self).__init__(5, 5, 1110, 620)
        self.__parent = parent

        self.__buttonnames__ = {
            "choose": (
                self.getMenuSceneParent().getMenuParent().switchToLevels,
                (75, 225)
            ),
            "extra": (
                self.getMenuSceneParent().getMenuParent().switchToExtra,
                (75, 310)
            ),
            "about": (
                self.getMenuSceneParent().getMenuParent().switchToAbout,
                (75, 395)
            ),
            "exit": (
                self.getMenuSceneParent().getMenuParent().close,
                (75, 480)
            )
        }

        self.__buttons = []

        for i in self.__buttonnames__:
            b = Button.Button(self, i, 274, 74, self.__buttonnames__[i][0])
            self.__buttons.append(self.addWidget(b))
            self.__buttons[-1].setPos(*self.__buttonnames__[i][1])

        a = self.addPixmap(QtGui.QPixmap("resource/title.png"))
        a.setPos(0, 0)

        a = self.addPixmap(QtGui.QPixmap("resource/logo.jpg"))
        a.setPos(600, 300)

        a = self.addPixmap(QtGui.QPixmap("resource/some.png"))
        a.setPos(450, 200)

    def some(self):
        pass

    def getMenuSceneParent(self):
        return self.__parent
