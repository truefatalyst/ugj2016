#!/usr/bin/python3

from PyQt4 import QtGui, QtCore
from . import Button
from . import Font
from . import Achievement
from engine import leveleng

class ExtraScene(QtGui.QGraphicsScene):

    def __init__(self, parent):
        super(ExtraScene, self).__init__(5, 5, 1110, 620)
        self.__parent = parent
        self.__chooser = leveleng.LevelEng()
        if not self.__chooser.load():
            self.__chooser.newCur()
        self.__ach = self.__chooser.getAchiv()
        arh = self.__ach.getArchive()
        stat = self.__ach.getStat()

        s = QtGui.QScrollArea()
        s.setFixedHeight(380)
        s.setFixedWidth(1070)
        s.setStyleSheet(
            """
            background-color: transparent;
            """
        )
        a = QtGui.QWidget()
        l = QtGui.QGridLayout()
        l.setSpacing(40)
        l.setContentsMargins(40, 20, 20, 20)
        l.setHorizontalSpacing(40)
        a.setLayout(l)
        x = y = 0
        ok = False
        for i in sorted(arh):
            l.addWidget(Achievement.Achievement(i, arh[i][0], arh[i][1], stat[i]), y, x)
            if ok:
                x = 0
                y += 1
                ok = not ok
            else:
                x += 1
                ok = not ok
        s.setWidget(a)
        a = self.addWidget(s)
        a.setPos(25, 120)

        a = self.addPixmap(QtGui.QPixmap("resource/achivblock.png"))
        a.setPos(25, 10)


        a = Button.Button(
            self.getExtraSceneParent().getExtraParent(),
            "exitm",
            274,
            74,
            self.getExtraSceneParent().getExtraParent().returnToMenu
        )
        a = self.addWidget(a)
        a.setPos(800, 525)


    def getExtraSceneParent(self):
        return self.__parent