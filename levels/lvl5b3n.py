poslst = [i for i in range(15)]
fdescr = [
    'Последний уровень...',
    '',
    'Скучали?',
    'Нет, подсказок здесь нет.',
    'Это же Хардкор.',
    'Я и не уверен, что это возможно решить.',
    'Просто меня вдруг посетила простая мысль.',
    'Если ты "молчишь", тебя нет.',
    'Если "говоришь", ты живой.',
    '','','','','','','','','','','',''
]
fpres = 'count;*;avg;*;x;-;maxm;*;moda'.split(';')
posfixed = [2,4,5,8]

def f(x, elem):
    return elem.count*elem.avg*x - elem.maxm*elem.moda