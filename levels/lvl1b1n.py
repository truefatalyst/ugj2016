poslst = [i for i in range(15)]
fdescr = [
    'Первые 2 значения всегда верны.',
    'Знаменитая рекуррентная формула.'
]
fpres = 'f;(;x;-;1;);+;f;(;x;-;2;)'.split(';')
posfixed = [1,3,4,7,10,11,12]

def f(x, elem):
    a = elem.usrlst[0]
    b = elem.usrlst[1]
    res = [a, b]
    i = 2
    while i <= x:
        b = a + b
        a = b - a
        res.append(b)
        i += 1
    return res[x]