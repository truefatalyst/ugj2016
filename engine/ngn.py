path = 'levels.'

class NGN:
    forchoice = (
        '',
        'f',
        'x',
        '+',
        '-',
        '*',
        '(',
        ')',
        'mxcnt',
        'unic',
        'avg',
        'moda',
        'maxm',
        'minm',
        'count'
    )
    choicelist = {
        'f': "Сама Функция",
        'x': "Аргумент",
        '+': "Сложение",
        '-': "Вычитание",
        '*': "Умнжоние",
        'mxcnt': 'Кол-во самого частого числа в выборке',
        'unic': 'Кол-во уникальных чисел в выборке',
        'avg': 'Среднее арифметическое чисел из выборки',
        'moda': 'Самое частое число в выборке (если не одно, то меньшее из них)',
        'maxm': 'Максимальное число из выборки',
        'minm': 'Минимальное число из выборки',
        'count': 'Номер текущего эксперимента'
    }

    def __init__(self):
        self.f = None
        self.posfixed = []
        self.fdescr = ['']
        self.fpres = ''
        self.poslst = []
        self.history = []
        self.usrlst = []
        self.count = 0
        self.mxcnt = 0
        self.unic = 0
        self.avg = 0
        self.moda = 0
        self.maxm = 0
        self.minm = 0

    def loadfromfile(self, n1, n2):
        s = [ path+'lvl'+str(n1)+'b'+str(n2)+'n' ]
        try:
            lvl = getattr(list(map(__import__, s))[0], "lvl{}b{}n".format(n1, n2))
        except:
            raise ValueError("Нет такого уровня!")
        self.f = lvl.f
        self.fpres = lvl.fpres
        self.posfixed = lvl.posfixed
        self.fdescr = lvl.fdescr
        self.poslst = lvl.poslst

    def check_function(self, upres):
        t = [self.fpres[i] for i in range(len(self.fpres)) if i not in self.posfixed]
        return t == upres

    # вычисление всех переменных
    def fillparam(self):
        self.calc_count()
        self.calc_avg()
        self.calc_unic()
        self.calc_mxcnt()
        self.calc_moda()
        self.calc_maxm()
        self.calc_minm()

    # для вычисления всей информации по данному usrlst
    def fillstat(self, usrlst):
        self.usrlst = usrlst
        self.fillparam()
        self.addtohistory()

    def calc_moda(self):
        self.moda = min([i for i in self.usrlst if self.usrlst.count(i) == self.mxcnt])

    def calc_minm(self):
        self.minm = min(self.usrlst)

    def calc_maxm(self):
        self.maxm = max(self.usrlst)

    def calc_count(self):
        self.count += 1

    def calc_avg(self):
        self.avg = round(sum((i for i in self.usrlst))/len(self.usrlst))

    def calc_unic(self):
        self.unic = len(set(self.usrlst))

    def calc_mxcnt(self):
        self.mxcnt = max((self.usrlst.count(i) for i in self.usrlst))

    def addtohistory(self):
        t = (self.usrlst, self.getparam())
        self.history.append(t)

    def gethistory(self):
        return self.history

    def gettry(self, n):
        if n < 1:
            raise ValueError("Wrong N!")
        if n > len(self.history):
            raise ValueError("Wrong N!")
        return self.history[n-1]

    # возращает список позиций в poslst, в которых значение угадано верно
    def getguessed(self, htry):
        templst = self.usrlst
        self.usrlst = htry[0]
        tempdict = self.getparam()
        param = htry[1]
        self.count = param['count']
        self.mxcnt = param['mxcnt']
        self.unic = param['unic']
        self.avg = param['avg']
        self.moda = param['moda']
        self.maxm = param['maxm']
        self.minm = param['minm']
        res = tuple(i for i in range(len(self.poslst)) if self.f(self.poslst[i], self) == self.usrlst[i])
        self.usrlst = templst
        self.count = tempdict['count']
        self.mxcnt = tempdict['mxcnt']
        self.unic = tempdict['unic']
        self.avg = tempdict['avg']
        self.moda = tempdict['moda']
        self.maxm = tempdict['maxm']
        self.minm = tempdict['minm']
        return res

    def getparam(self):
        return {
            'count':self.count,
            'mxcnt':self.mxcnt,
            'unic':self.unic,
            'avg':self.avg,
            'moda':self.moda,
            'maxm':self.maxm,
            'minm':self.minm
        }

    def getfunc(self):
        return (self.fpres, self.posfixed, self.poslst, self.fdescr)