#!/usr/bin/python3

import pickle

class Achiv:
    __arch = {
        'Третий лишний': ('Пройти все уровни в блоке, кроме третьего', 'resource/third.jpg'),
        'Бифуркация': ('Добраться до блока "Хардкор"','resource/bifur.jpg'),
        'Аспирин': ('Вы точно хорошо себя чувствуете? (Провести 150 экпериментов на одном уровне)', 'resource/aspirin.jpg'),
        'Играть второстепенным персонажем': ('Пройти обучение', 'resource/anotherone.jpg'),
        'Изнанка': ('Пройти игру', 'resource/reverse.jpg'),
        'Теснота': ('Разгадать функцию, пользуясь только первыми двумя значениями в поле для экспериментов', 'resource/tesno.jpg'),
        'Розовый слоник': ('?', 'resource/pinkelephant.jpg')
    }
    def __init__(self, parent):
        self.__parent = parent
        self.__stat = None

    def load(self):
        try:
            with open("achiv", "rb") as fin:
                self.__stat = pickle.load(fin)
        except FileNotFoundError:
            return False
        if (type(self.__stat) is not dict) or ("md5" not in self.__stat):
            return False
        if self.__stat["md5"] != self.__parent.getmd5():
            return False
        return True

    def setNewStat(self):
        self.__stat = { i: False for i in self.__arch }
        self.checkAfter()
        self.setmd5()

    def checkAfter(self):
        r = []
        r.extend(self.checkAnotherOne())
        r.extend(self.checkReverse())
        r.extend(self.checkBif())
        r.extend(self.checkPassed())
        return r

    def checkAnotherOne(self):
        if self.__stat['Играть второстепенным персонажем']:
            return []
        cur = self.__parent.getCur()
        if cur != 1:
            self.__stat['Играть второстепенным персонажем'] = True
            self.save()
            return ['Играть второстепенным персонажем']
        return []

    def checkReverse(self):
        if self.__stat['Изнанка']:
            return []
        cur = self.__parent.getCur()
        all = self.__parent.getAll()
        if cur == (len(all)+1):
            self.__stat['Изнанка'] = True
            self.save()
            return ['Изнанка']
        return []

    def checkBif(self):
        if self.__stat['Бифуркация']:
            return []
        cur = self.__parent.getCur()
        all = self.__parent.getAll()
        for i in range(cur):
            if all[i]["name"] == "V. Хардкор":
                self.__stat['Бифуркация'] = True
                self.save()
                return ['Бифуркация']
        return []

    def checkPassed(self):
        if self.__stat['Третий лишний']:
            return []
        cur = self.__parent.getCur()
        all = self.__parent.getAll()
        passed = self.__parent.getPassed()
        passed.sort()
        if all[cur-1]["levels"] < 3:
            return []
        k = [i for i in range(1, all[cur - 1]["levels"] + 1)]
        k.remove(3)
        if k == passed:
            self.__stat['Третий лишний'] = True
            self.save()
            return ['Третий лишний']
        return []

    def checkAsp(self, k):
        if self.__stat['Аспирин']:
            return []
        if k == 150:
            self.__stat['Аспирин'] = True
            self.save()
            return ['Аспирин']
        return []

    def checkTesn(self, hist):
        if self.__stat['Теснота'] or (not len(hist)):
            return []
        i = 0
        while i < len(hist):
            t = hist[i][0][2:]
            n = [j for j in t if j]
            if len(n): return []
            i += 1
        self.__stat['Теснота'] = True
        self.save()
        return ['Теснота']

    def checkPink(self, st):
        if self.__stat['Розовый слоник']:
            return []
        if st.lower().find("розовый слоник") != -1:
            self.__stat['Розовый слоник'] = True
            self.save()
            return ['Розовый слоник']
        if st.lower().find("розовыйслоник") != -1:
            self.__stat['Розовый слоник'] = True
            self.save()
            return ['Розовый слоник']
        return []

    def setmd5(self):
        self.__stat["md5"] = self.__parent.getmd5()
        self.save()

    def save(self):
        with open("achiv", "wb") as fout:
            pickle.dump(self.__stat, fout)

    def getArchive(self):
        return self.__arch

    def getStat(self):
        return self.__stat